# -*- coding: utf-8 -*-

import numpy as np


from pylab import *

# Cost function parameters.
c0 = 1.0
c1 = 1.0

# Demand function parameter.
kappa = 1.0

p = -4.0/3.0*c0
q = -16.0/27.0*c0**3-4.0*c1*kappa

Delta = -(4.0*p**3+27.0*q**2)

u = np.power((-q+np.sqrt(-Delta/27.0))/2.0,1.0/3.0)
v = np.power((-q-np.sqrt(-Delta/27.0))/2.0,1.0/3.0)

z = u+v
pm = z+2.0/3.0*c0 # Real root of the third order polynomial determining the optimal price.
qm = kappa*pm**(-2.0)

n = 200
p = np.linspace(2.000,3.200,n)
q = kappa*p**(-2.0)
Recette = p*q
RecetteMarginale = -kappa/p**2.0
CoutMarginal = -2.0*kappa*(c0+2.0*c1*kappa/p**2.0)/p**3.0

xlower = 1.9;
xupper = 3.2;
ylower = -.4;
yupper =  .3;


tikz = open('exercice-3-question-3.tikz', 'w')

tikz.write('\\hspace{-.75cm}\\begin{tikzpicture}[scale=9]\n\n')
tikz.write('\\draw [<->] (' + str(xlower) + ',' + str(yupper) + ') node[left] {$q$} -- (' + str(xlower) + ',' + str(ylower) + ') -- (' + str(xupper) +  ',' + str(ylower) + ') node[right] {$p$};\n\n');

# Demande
tikz.write('\\draw [black] ')
for i in range(n-1):
    tikz.write('(' + str(p[i]) + ', ' + str(q[i]) + ') -- ')
tikz.write('(' + str(p[n-1]) + ', ' + str(q[n-1]) + ');\n\n')

# Recette marginale
tikz.write('\\draw [black, thick] ')
for i in range(n-1):
    tikz.write('(' + str(p[i]) + ', ' + str(RecetteMarginale[i]) + ') -- ')
tikz.write('(' + str(p[n-1]) + ', ' + str(RecetteMarginale[n-1]) + ');\n\n')

# Coût marginal
tikz.write('\\draw [black, thick] ')
for i in range(n-1):
    tikz.write('(' + str(p[i]) + ', ' + str(CoutMarginal[i]) + ') -- ')
tikz.write('(' + str(p[n-1]) + ', ' + str(CoutMarginal[n-1]) + ');\n\n')

# Décision optimale du monopole
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(pm) + ',' + str(ylower) + ') -- (' + str(pm) + ',' + str(qm) + ') ;\n')
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(xlower) + ',' + str(qm) + ') -- (' + str(pm) + ',' + str(qm) + ') ;\n')
tikz.write('\\draw (' + str(pm) + ', ' + str(ylower) + ') node[below] {$p^m$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(qm) + ') node[left] {$q^m$};\n\n')

# Noms des courbes
tikz.write('\\draw (' + str(xlower) + ', ' + str(q[0]) + ') node[right] {$D(p)$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(RecetteMarginale[0]+.025) + ') node[right] {$\mathcal R_m(p)$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(CoutMarginal[0]+.035) + ') node[right] {$\mathcal C_m(p)$};\n\n')

# Zéro
tikz.write('\\draw [red, thin] (' + str(xlower)  + ',' + str(0) + ') -- (' + str(xupper) + ',' + str(0) + ') ;\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(0) + ') node[left] {$0$};\n\n')

tikz.write('\\end{tikzpicture}')
tikz.close()


q = np.linspace(0.1,1,n)
p = kappa**(1.0/2.0)/sqrt(q)
Recette = p*q
RecetteMarginale = kappa**(1.0/2.0)/sqrt(q)/2.0
CoutMarginal = c0 + 2*c1*q

xlower = 0.0;
xupper = 1.1;
ylower = 0.4;
yupper = 3.5;


tikz = open('exercice-3-question-7.tikz', 'w')

tikz.write('\\hspace{-1.25cm}\\begin{tikzpicture}[xscale=10,yscale=2]\n\n')
tikz.write('\\draw [<->] (' + str(xlower) + ',' + str(yupper) + ') node[left] {$p$} -- (' + str(xlower) + ',' + str(ylower) + ') -- (' + str(xupper) +  ',' + str(ylower) + ') node[right] {$q$};\n\n');

# Demande
tikz.write('\\draw [black,thick] ')
for i in range(n-1):
    tikz.write('(' + str(q[i]) + ', ' + str(p[i]) + ') -- ')
tikz.write('(' + str(q[n-1]) + ', ' + str(p[n-1]) + ');\n\n')

# Recette marginale
tikz.write('\\draw [black, thick] ')
for i in range(n-1):
    tikz.write('(' + str(q[i]) + ', ' + str(RecetteMarginale[i]) + ') -- ')
tikz.write('(' + str(q[n-1]) + ', ' + str(RecetteMarginale[n-1]) + ');\n\n')

# Coût marginal
tikz.write('\\draw [black, thick] ')
for i in range(n-1):
    tikz.write('(' + str(q[i]) + ', ' + str(CoutMarginal[i]) + ') -- ')
tikz.write('(' + str(q[n-1]) + ', ' + str(CoutMarginal[n-1]) + ');\n\n')

# Noms des courbes
tikz.write('\\draw (' + str(xlower) + ', ' + str(p[0]) + ') node[right] {$P(q)$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(RecetteMarginale[0]+.095) + ') node[right] {$R_m(q)$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(CoutMarginal[0]+.1) + ') node[right] {$C_m(q)$};\n\n')

# Décision optimale du monopole
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(qm) + ',' + str(ylower) + ') -- (' + str(qm) + ',' + str(pm) + ') ;\n')
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(xlower) + ',' + str(pm) + ') -- (' + str(qm) + ',' + str(pm) + ') ;\n')
tikz.write('\\draw (' + str(qm) + ', ' + str(ylower) + ') node[below] {$q^m$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(pm) + ') node[left] {$p^m$};\n\n')

# Décision optimale du dictateur bienveillant
idzero = np.argmin(np.absolute(p-CoutMarginal))
qc = q[idzero]
pc = p[idzero]
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(qc) + ',' + str(ylower) + ') -- (' + str(qc) + ',' + str(pc) + ') ;\n')
tikz.write('\\draw [loosely dashed, gray, thin] (' + str(xlower) + ',' + str(pc) + ') -- (' + str(qc) + ',' + str(pc) + ') ;\n')
tikz.write('\\draw (' + str(qc) + ', ' + str(ylower) + ') node[below] {$q^c$};\n')
tikz.write('\\draw (' + str(xlower) + ', ' + str(pc) + ') node[left] {$p^c$};\n\n')

tikz.write('\\end{tikzpicture}')
tikz.close()
