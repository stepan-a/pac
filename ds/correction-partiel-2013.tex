\documentclass[10pt,a4paper,notitlepage]{article}
\usepackage{amsmath,amssymb,amsbsy}
\usepackage[french]{babel}

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{palatino}
\usepackage{manfnt}
\usepackage{hyperref}
\usepackage{nicefrac}
\usepackage{color}

\usepackage[active]{srcltx}
\usepackage{scrtime}

\newcommand{\exercice}[1]{\textsc{\textbf{Exercice}} #1.}
\newcommand{\question}[1]{\textbf{(#1)}}
\newcommand{\Question}[1]{\textsc{\textbf{Question}} #1.}
\setlength{\parindent}{0cm}

\begin{document}

\title{\textsc{Économie de la concurrence}}
\author{\textsc{Université du Maine (Éléments de correction du partiel, L3)}}
\date{}

\maketitle

Vous  trouverez  les   réponses  détaillées  dans  le   cours  et  les
corrections  des  exercices des  fiches  de  travaux dirigés.   Je  me
contente  ici de  donner  brièvement les  solutions  aux questions  de
l'exercice.\newline

Soit  un marché  servi par  un  monopole avec  $N$ consommateurs.   On
suppose que la fonction de coût  du monopole est linéaire et on notera
$c$  le  coût  marginal.\newline  

\textcolor{red}{\textbf{Remarque.} La fonction de coût est donc de la forme :
\[
C(q) = c \times q
\]
}
Pour   commencer  on  suppose  que  les  $N$
consommateurs sont identiques. La fonction de demande est :
\[
q = \kappa p^{-\epsilon}
\]
\question{1} Interprétez le  paramètre $\epsilon$.  Quelle restriction
faut-il poser sur ce  paramètre ? Pourquoi ?\newline

\textcolor{red}{Le paramètre $\epsilon$ est l'élasticité prix de la demande. En effet on a :
\[
\begin{split}
  -\frac{\mathrm d q}{\mathrm d p}\frac{p}{q} &= \epsilon\kappa p^{-\epsilon-1} \frac{p}{q}\\
  &=\epsilon \kappa p^{-\epsilon-1} \frac{p}{\kappa p^{-\epsilon}}\\
  &=\epsilon
\end{split}
\]
Il faut supposer que $\epsilon>1$,  autrement le programme du monopole
est mal défini. En effet si  la demande réagit trop peu aux variations
du  prix, le  monopole peut  indéfiniment  augmenter son  prix car  sa
recette est alors une fonction monotone croissante du prix.}\newline

\question{2} Quel  serait le  prix concurrentiel  sur ce  marché ?  On
notera ce prix $p^c$.\newline

\textcolor{red}{Sur un  marché concurrentiel  le prix égalise  le coût
  marginal, $p^c = c$.}\newline

\question{3} Calculez le surplus des consommateurs et le surplus total
dans ce cas.\newline 

\textcolor{red}{On a par définition :
\[
S_{CP}^C = \int_{p^c}^{\infty} D(p)\mathrm d p = \kappa \frac{c^{1-\epsilon}}{\epsilon-1}
\]
le   surplus  des   consommateurs  si   le  marché   est  parfaitement
concurrentiel. Puisque  le coût  marginal, le  surplus (profit)  de la
firme est  nul. Le surplus des  consommateurs est donc aussi,  dans ce
cas, le surplus total.}\newline

\question{4} Caractérisez  le comportement du monopole et
déterminez  son  prix  $p^m$.\newline  

\textcolor{red}{Le  monopole  maximise  son  profit.  À  l'optimum  il
  égalise sa recette marginale avec le coût marginal. On obtient :
\[
p^m = \frac{\epsilon}{\epsilon-1}c
\]
le prix du monopole.
}

\question{5} Calculez le surplus des consommateurs et le surplus total
dans ce cas.\newline

\textcolor{red}{Dans cette situation le surplus des consommateurs est :
\[
S_{M}^C = \int_{p^m}^{\infty} D(p)\mathrm d p = \kappa \left(\frac{\epsilon}{\epsilon-1}\right)^{1-\epsilon}\frac{c^{1-\epsilon}}{\epsilon-1} < S_{M}^C 
\]
Le profit (surplus) de la firme est :
\[
\Pi_M = \left(p^m- c\right)q^m = \kappa \left(\frac{\epsilon}{\epsilon-1}\right)^{-\epsilon} \frac{c^{1-\epsilon}}{\epsilon-1}
\]
Relativement  au  cas  parfaitement  concurrentiel,  on  constate  une
diminution du surplus des consommateurs et une augmentation du surplus
de  la  firme.  Nous  avons  montré  en cours  (et  dans  la fiche  de
\textsc{td} n°3  dans le cas de  ces fonctions de demande  et de coût)
que l'augmentation du profit est plus  faible que la baisse du surplus
des consommateurs. C'est  en ce sens que la présence  du monopole pose
un problème. Le surplus total est :
\[
S_M = S_{M}^C + \Pi_M = S_{CP}^C \left(\frac{\epsilon}{\epsilon-1}\right)^{-\epsilon}\left(1+\frac{\epsilon}{\epsilon-1}\right)
\]
}

On suppose maintenant que  les consommateurs sont hétérogènes : chaque
consommateur  $i   $est  caractérisé  par  une   fonction  de  demande
spécifique :
\[
q_i = \kappa p^{-\epsilon_i}
\]
où  $\kappa$   est  une   constante  positive  et   $\epsilon_i$  (pour
$i=1,\dots,N$) vérifient la restriction  définie dans la question (1).\newline


\question{6} Sous  quelles conditions le monopole  peut-il discriminer
par les prix (en supposant  qu'il n'existe pas de contrainte légale) ?\newline

\textcolor{red}{Le monopole  peut discriminer par  les prix s'il observe  les demandes
individuelles  et  si  les  consommateurs  n'ont  pas  la  possibilité
d'échanger le bien sur un marché secondaire.}\newline

\question{7}  En supposant  que  le monopole  adopte une  tarification
linéaire, décrivez le comportement du monopole (\textit{ie} déterminez
le prix $p_i^m$ que le monopole  va proposer à chaque consommateur) et
calculez  son   profit.\newline   

\textcolor{red}{Le monopole maximise son profit sur chaque segment du marché. On trouve :
\[
p_i^m = \frac{\epsilon_i}{\epsilon_i-1}c
\]
le profit généré sur le segment de marché $i$ :
\[
\Pi_{M,i} = \left(p_i^m- c\right)q_i^m = \kappa \left(\frac{\epsilon_i}{\epsilon_i-1}\right)^{-\epsilon_i} \frac{c^{1-\epsilon_i}}{\epsilon_i-1}
\]
et le profit agrégé :
\[
\Pi_{M} = \kappa \sum_{i=1}^N\left(\frac{\epsilon_i}{\epsilon_i-1}\right)^{-\epsilon_i} \frac{c^{1-\epsilon_i}}{\epsilon_i-1}
\]
}

\question{8}   Déterminez  le   surplus  des consommateurs et  le surplus global  dans ce cas.\newline

\textcolor{red}{Le surplus du consommateur $i$ est :
\[
S_{M,i}^C = \kappa \left(\frac{\epsilon_i}{\epsilon_i-1}\right)^{1-\epsilon_i}\frac{c^{1-\epsilon_i}}{\epsilon_i-1} 
\]
En sommant sur $i$, on obtient le surplus agrégé des consommateurs :
\[
S_{M}^C = \kappa \sum_{i=1}^N \left(\frac{\epsilon_i}{\epsilon_i-1}\right)^{1-\epsilon_i}\frac{c^{1-\epsilon_i}}{\epsilon_i-1} 
\]
Le surplus global est alors :
\[
S_M = S_{M}^C + \Pi_{M} = \kappa \sum_{i=1}^N  \frac{c^{1-\epsilon_i}}{\epsilon_i-1} \left(\frac{\epsilon_i}{\epsilon_i-1}\right)^{-\epsilon_i}\left(1+\frac{\epsilon_i}{\epsilon_i-1}\right) 
\]
}

\question{9} Quel serait le  surplus des  consommateurs dans  le cas  d'une tarification
concurrentielle ?\newline

\textcolor{red}{Dans le cas d'une tarification au coût marginal, le surplus des consommateurs serait :
\[
S_{CP}^C = \kappa \sum_{i=1}^N  \frac{c^{1-\epsilon_i}}{\epsilon_i-1}
\]
}

\question{10} On suppose  maintenant que le monopole  a la possibilité
d'adopter  un tarif  non linéaire  de  la forme  $T(q_i) =  A_i +  p_i
q_i$.  Proposez  un   barème  de  prix  qui  maximise   le  profit  du
monopole.\newline

\textcolor{red}{Le  monopole a  intérêt  à proposer  un tarif  marginal  égal au  prix
concurrentiel,  $p_i  =  c  \,  \forall\,  i=1,\dots,N$,  de  façon  à
maximiser la demande exprimée par les consommateurs et, pour accéder à
ce tarif  marginal, à demander  une prime, $A_i$, spécifique  à chaque
consommateur, égale au surplus du consommateur de type $i$ :
\[
A_i = S_{CP,i}^C = \kappa  \frac{c^{1-\epsilon_i}}{\epsilon_i-1}
\]
}

\question{11} Quel  est le  surplus global  dans ce  cas ? Commentez.\newline

\textcolor{red}{Avec la  tarification proposée  plus haut  le monopole
  absorbe la  totalité du  surplus des  consommateurs sans  générer de
  perte (car  le comportement des  consommateurs est déterminé  par le
  tarif marginal). Ainsi le surplus global est égal au surplus des consommateurs :
  \[
  S_{MTNL} = \kappa \sum_{i=1}^N  \frac{c^{1-\epsilon_i}}{\epsilon_i-1}
  \]
  Avec  cette tarification  non linéaire  la présence  du monopole  ne
  génère pas de perte sociale.}

\end{document}